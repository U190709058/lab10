package stacks;

public class StackImpl implements Stack{
    StackVar top;

    @Override
    public void push(Object i) {
        if (!empty()) {
            StackVar tempItem = new StackVar(i);
            tempItem.setNext(this.top);
            this.top = tempItem;
        } else this.top = new StackVar(i);
    }

    @Override
    public Object pop() {
        if (!empty()) {
            if (this.top.getNext() != null) {
                StackVar returnItem = this.top;
                this.top = returnItem.getNext();
                return returnItem.item;
            }
            Object returnItem = this.top.item;
            this.top = null;
            return returnItem;
        }
        return null;
    }
    @Override
    public boolean empty() {
        return this.top == null;
    }
}

class StackVar {
    Object item;
    private StackVar next;

    public StackVar getNext() {
        return next;
    }

    public void setNext(StackVar next) {
        this.next = next;
    }

    public StackVar(Object item){
        this.item = item;
    }

}